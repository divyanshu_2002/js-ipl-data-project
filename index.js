const csv = require('csvtojson')

async function csvToJsonConverter(pathofcsv) {
    try {
        const jsonData = await csv().fromFile(pathofcsv)
        return jsonData;
    }
    catch (error) {
        return null;
    }

}

module.exports = csvToJsonConverter;
