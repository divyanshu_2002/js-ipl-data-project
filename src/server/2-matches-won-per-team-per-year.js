const csvToJsonConverter = require("../../index");
const fs = require("fs");
const matchesFilePath = '../data/matches.csv';
let matchJsonData = {};

const findMatchWonPerTeamPerYear = async () => {
    try {
        matchJsonData = await csvToJsonConverter(matchesFilePath);
        if (!matchJsonData || Array.isArray(matchJsonData) == false) {
            console.log("Error in getting data in the form of array of objects from csvToJsonConverter method");
            return;
        }
    }
    catch (error) {
        console.log(error);
    }

    const matchesWonByTeamPerYear = {};

    for (let index = 0; index < matchJsonData.length; index++) {
        const season = matchJsonData[index].season;
        const winnerTeam = matchJsonData[index].winner;
        if (matchesWonByTeamPerYear[season]) {
            if (matchesWonByTeamPerYear[season][winnerTeam]) {
                matchesWonByTeamPerYear[season][winnerTeam] += 1;
            }
            else {
                matchesWonByTeamPerYear[season][winnerTeam] = 1;
            }
        }
        else {
            matchesWonByTeamPerYear[season] = {};
            matchesWonByTeamPerYear[season][winnerTeam] = 1;
        }
    }

    try {
        fs.writeFileSync("../public/output/matches-won-per-team-per-year.json", JSON.stringify(matchesWonByTeamPerYear, null, 2));
        console.log("Output file  is created");

    }
    catch (error) {
        console.log("error in creating the output file");
    }

}
findMatchWonPerTeamPerYear();