// Top 10 economical bowlers in the year 2015

const csvToJsonConverter = require("../../index");
const matchesFilePath = '../data/matches.csv';
const deliveriesFilePath = '../data/deliveries.csv';
let matchJsonData = [];
let deliveryJsonData = [];
const fs = require("fs");


const topTenEconomicalBowler = async () => {
    try {
        deliveryJsonData = await csvToJsonConverter(deliveriesFilePath);
        matchJsonData = await csvToJsonConverter(matchesFilePath);
        if (!deliveryJsonData || !matchJsonData || Array.isArray(deliveryJsonData) == false || Array.isArray(matchJsonData) == false) {
            console.log("Error in getting data from csvToJsonConverter method");
            return;
        }
    }
    catch (error) {
        console.log("error occured!!", error);
    }

    const playerAndRuns = [];

    for (let index = 0; index < matchJsonData.length; index++) {
        const year = matchJsonData[index].season;
        if (year == 2015) {
            const matchId = matchJsonData[index].id;

            for (let over = 0; over < deliveryJsonData.length; over++) {

                if (deliveryJsonData[over].match_id == matchId) {
                    const bowlerName = deliveryJsonData[over].bowler;
                    const runsByBowler = parseInt(deliveryJsonData[over].total_runs);
                    const isOver = deliveryJsonData[over].ball;
                    if (playerAndRuns[bowlerName]) {
                        playerAndRuns[bowlerName][0] += runsByBowler;

                        if (isOver == 1) {
                            playerAndRuns[bowlerName][1] += 1;

                        }

                    }
                    else {
                        playerAndRuns[bowlerName] = [0, 0];
                        playerAndRuns[bowlerName][0] = runsByBowler;
                        playerAndRuns[bowlerName][1] = 1;
                    }
                }
            }
        }
    }

    for (bowler in playerAndRuns) {
        const arrayOfDetails = playerAndRuns[bowler];
        const runs = arrayOfDetails[0];
        const overs = arrayOfDetails[1];
        const economyOfBowler = (runs / overs).toFixed(2);
        arrayOfDetails.push(economyOfBowler);
    }

    const objectEntries = Object.entries(playerAndRuns);

    objectEntries.sort((a, b) => {
        let economyA = parseFloat(a[1][2]);
        let economyB = parseFloat(b[1][2]);
        return economyA - economyB;
    });

    const topTenBowler = [];
    for (let index = 0; index < 10; index++) {
        topTenBowler.push(objectEntries[index][0]);
    }

    try {
        fs.writeFileSync("../public/output/top-ten-economical-bowler.json", JSON.stringify(topTenBowler, null, 2));
    }
    catch (error) {
        console.log("error occured while writing", error.message);
    }
}
topTenEconomicalBowler();