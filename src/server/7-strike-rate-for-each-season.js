const csvToJsonConverter = require("../../index");
const deliveriesFilePath = "../data/deliveries.csv";
let matchesFilePath = "../data/matches.csv";

const fs = require("fs");

const findStrikeRateOfChGayle = async () => {
    try {
        deliveriesJsonData = await csvToJsonConverter(deliveriesFilePath);
        matchJsonData = await csvToJsonConverter(matchesFilePath);

        if (!deliveriesJsonData || Array.isArray(deliveriesJsonData) == false || !deliveriesJsonData || Array.isArray(matchJsonData) == false) {
            console.log("Returned type of the csvToJsonConverter must be array");
            return;
        }
    }
    catch (error) {
        console.log("error in converting into json formet", error.message);
    }

    const strikeRatePerYear = {};

    const uniqueYears = new Set();

    for (let matches = 0; matches < matchJsonData.length; matches++) {
        const yearOfMatch = matchJsonData[matches].season;
        uniqueYears.add(yearOfMatch);
    }

    for (years of uniqueYears) {
        let noOfInnings = 0;
        let totalRun = 0;
        for (let matches = 0; matches < matchJsonData.length; matches++) {

            const yearOfMatch = matchJsonData[matches].season;
            const matchId = matchJsonData[matches].id;
            if (yearOfMatch == years) {

                for (let index = 0; index < deliveriesJsonData.length; index++) {
                    if (deliveriesJsonData[index].match_id == matchId) {
                        const batsmanName = deliveriesJsonData[index].batsman.trim();
                        const runs = parseInt(deliveriesJsonData[index].batsman_runs);
                        if (batsmanName.includes("CH Gayle")) {
                            noOfInnings++;
                            totalRun += runs;
                        }
                    }

                }
            }
        }
        if (totalRun == 0) {
            strikeRatePerYear[years] = 0;
        }
        else {
            const strikeRate = parseFloat((totalRun * 100 / noOfInnings).toFixed(2));
            strikeRatePerYear[years] = strikeRate;
        }
    }

    try {
        fs.writeFileSync("../public/output/strike-rate-for-each-season.json", JSON.stringify(strikeRatePerYear, null, 2));
        console.log("file created in output folder");
    }
    catch (error) {
        console.log("error occured while writing", error.message);
    }

}
findStrikeRateOfChGayle();