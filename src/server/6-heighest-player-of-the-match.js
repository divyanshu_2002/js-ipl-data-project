const csvToJsonConverter = require("../../index");
const fs = require("fs");
const matchesFilePath = '../data/matches.csv';
let matchJsonData = [];

const findHeighestPlayerOfTheMatch = async () => {
    try {
        matchJsonData = await csvToJsonConverter(matchesFilePath);
        if (!matchJsonData || Array.isArray(matchJsonData) == false) {
            console.log("Error in getting data from csvToJsonConverter method");
            return;
        }
    }
    catch (error) {
        console.log("error in converting the data from csv to json format", error);
    }

    const heighestPlayerOfMatchCount = {};

    for (let index = 0; index < matchJsonData.length; index++) {
        const playerOfTheMatch = matchJsonData[index].player_of_match;
        const year = matchJsonData[index].season;
        if (heighestPlayerOfMatchCount[year]) {
            if (heighestPlayerOfMatchCount[year][playerOfTheMatch]) {
                heighestPlayerOfMatchCount[year][playerOfTheMatch] += 1;
            }
            else {
                heighestPlayerOfMatchCount[year][playerOfTheMatch] = 1;
            }
        }
        else {
            heighestPlayerOfMatchCount[year] = {};
            heighestPlayerOfMatchCount[year][playerOfTheMatch] = 1;
        }
    }

    const heighestPlayerOfMatch = {};

    for (year in heighestPlayerOfMatchCount) {
        let maxTime = 0;
        let nameOfPlayer = "";
        const data = heighestPlayerOfMatchCount[year];
        for (player in data) {

            if (data[player] > maxTime) {
                maxTime = data[player];
                heighestPlayerOfMatch[year] = player;
            }

            if (data[player] == maxTime) {
                heighestPlayerOfMatch[year] = heighestPlayerOfMatch[year].concat(',', player);
            }
        }
    }

    try {
        fs.writeFileSync("../public/output/heighest-player-of-the-match.json", JSON.stringify(heighestPlayerOfMatch, null, 2));
        console.log("output file is created");
    }
    catch (error) {
        console.log("error in writing in the file", error.message);
    }
}

findHeighestPlayerOfTheMatch();