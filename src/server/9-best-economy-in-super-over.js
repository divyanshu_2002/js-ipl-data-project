const csvToJsonConverter = require("../../index");
const deliveriesFilePath = "../data/deliveries.csv";
const fs = require("fs");
let deliveryJsonData;

async function findBestEconomyInSuperOver() {
    try {
        deliveryJsonData = await csvToJsonConverter(deliveriesFilePath);
        if (!deliveryJsonData || Array.isArray(deliveryJsonData) == false) {
            console.log("Error in getting data from csvToJsonConverter method");
            return;
        }
    }
    catch (error) {
        console.log("error occured!!", error);
    }

    const runsAndOverOfEachBowler = {};

    for (let index = 0; index < deliveryJsonData.length; index++) {
        const oneOver = deliveryJsonData[index];
        const bowlerName = oneOver.bowler;
        if (oneOver.is_super_over == 1) {
            const runsGiven = parseInt(oneOver.total_runs);
            const isOver = deliveryJsonData[index].ball;
            if (runsAndOverOfEachBowler[bowlerName]) {
                runsAndOverOfEachBowler[bowlerName][1] += runsGiven;
                runsAndOverOfEachBowler[bowlerName][0] += 1;
            }
            else {
                runsAndOverOfEachBowler[bowlerName] = [0, 0];
                runsAndOverOfEachBowler[bowlerName][0] = 1;
                runsAndOverOfEachBowler[bowlerName][1] = runsGiven;
            }
        }
    }

    let bowlerWithBestEconomy = "";
    let bestEconomy = 50;
    for (bowler in runsAndOverOfEachBowler) {
        const overs = runsAndOverOfEachBowler[bowler][0];
        const runs = runsAndOverOfEachBowler[bowler][1];
        const economy = runs / ((overs) / 6);
        
        if (economy < bestEconomy) {
            bestEconomy = economy;
            bowlerWithBestEconomy = bowler;
        }

    }

    const obj = {};
    obj[`${bowlerWithBestEconomy}`] = bestEconomy
    try {
        fs.writeFileSync("../public/output/best-economy-in-super-overs.json", JSON.stringify(obj));
        console.log("output file is created");
    }
    catch (error) {
        console.log("error occured while writing", error.message);
    }

}
findBestEconomyInSuperOver();