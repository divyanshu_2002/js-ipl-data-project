const csvToJsonConverter = require("../../index");
const fs = require("fs");
const matchesFilePath = '../data/matches.csv';
let matchJsonData = [];

const findMatchPerYear = async () => {
    try {
        matchJsonData = await csvToJsonConverter(matchesFilePath);
        if (!matchJsonData || Array.isArray(matchJsonData) == false) {
            console.log("Error in getting data from csvToJsonConverter method");
            return;
        }
    }
    catch (error) {
        console.log("error in converting the data from csv to json format", error);
    }

    const matchPerYear = {};

    for (let index = 0; index < matchJsonData.length; index++) {
        if (matchPerYear[matchJsonData[index].season]) {
            matchPerYear[matchJsonData[index].season]++;
        }
        else {
            matchPerYear[matchJsonData[index].season] = 1;
        }
    }

    try {
        fs.writeFileSync("../public/output/matchPerYear.json", JSON.stringify(matchPerYear, null, 2));
        console.log("file is created");
    }
    catch (error) {
        console.log(error);
    }

}
findMatchPerYear();

