const csvToJsonConverter = require("../../index");
const matchesFilePath = '../data/matches.csv';
const fs = require("fs");
let matchJsonData = [];

const findWinnerOfBothTossAndMatch = async () => {
    try {
        matchJsonData = await csvToJsonConverter(matchesFilePath);
        if (!matchJsonData || Array.isArray(matchJsonData) == false) {
            console.log("Returned type of the csvToJsonConverter must be array");
            return;
        }
    }
    catch (error) {
        console.log("error in converting into json formet", error.message);
    }

    const winnerOfBothTossAndMatch = {};

    for (let index = 0; index < matchJsonData.length; index++) {
        const tossWinnerTeam = matchJsonData[index].toss_winner.trim();
        const matchWinnerTeam = matchJsonData[index].winner.trim();
        if (tossWinnerTeam == matchWinnerTeam) {
            if (winnerOfBothTossAndMatch[tossWinnerTeam]) {
                winnerOfBothTossAndMatch[tossWinnerTeam] += 1;
            }
            else {
                winnerOfBothTossAndMatch[tossWinnerTeam] = 1;
            }
        }
    }
    try {
        fs.writeFileSync("../public/output/both-toss-and-match-winner.json", JSON.stringify(winnerOfBothTossAndMatch, null, 2));
        console.log("output file created");
    }
    catch (error) {
        console.log("error occured in writing the file", error.message);
    }

}
findWinnerOfBothTossAndMatch();