const csvToJsonConverter = require("../../index");
const deliveriesFilePath = '../data/deliveries.csv'
const matchesFilePath = '../data/matches.csv';
const fs = require("fs");
let deliveryJsonData = [];
let matchJsonData = [];

const findExtraRuns = async () => {
    try {
        deliveryJsonData = await csvToJsonConverter(deliveriesFilePath);
        matchJsonData = await csvToJsonConverter(matchesFilePath);
        if (!deliveryJsonData || !matchJsonData || Array.isArray(deliveryJsonData) == false || Array.isArray(matchJsonData) == false) {
            console.log("Error in getting data from csvToJsonConverter method");
            return;
        }
    }
    catch (error) {
        console.log("error occured!!", error);
    }

    const extraRunsInYear2016 = {};
    for (let index = 0; index < matchJsonData.length; index++) {
        const year = matchJsonData[index].season;
        const idOfMatch = matchJsonData[index].id;
        if (year == 2016) {
            for (let over = 0; over < deliveryJsonData.length; over++) {
                if (deliveryJsonData[over].match_id == idOfMatch) {
                    const bowlingTeam = deliveryJsonData[over].bowling_team;
                    const extraRuns = parseInt(deliveryJsonData[over].extra_runs);
                    if (extraRunsInYear2016[bowlingTeam]) {
                        extraRunsInYear2016[bowlingTeam] += (extraRuns);
                    }
                    else {
                        extraRunsInYear2016[bowlingTeam] = (extraRuns);
                    }
                }
            }
        }
    }

    try {
        fs.writeFileSync("../public/output/extra-runs-in-2016.json", JSON.stringify(extraRunsInYear2016, null, 2));
        console.log("output file is created");
    }
    catch (error) {
        console.log("Error in creating the file", error.message);
    }

}
findExtraRuns();