const csvToJsonConverter = require("../../index");
const deliveriesFilePath = "../data/deliveries.csv";
const fs = require("fs");
let deliveryJsonData;

async function findHeighestNoOfTimesDismissed() {
    try {
        deliveryJsonData = await csvToJsonConverter(deliveriesFilePath);

        if (!deliveryJsonData || Array.isArray(deliveryJsonData) == false) {
            console.log("Error in getting data from csvToJsonConverter method");
            return;
        }
    }
    catch (error) {
        console.log("error occured!!", error);
    }

    const noOfTimesDismissedByOther = {};
    for (let index = 0; index < deliveryJsonData.length; index++) {
        const oneOver = deliveryJsonData[index];
        if (oneOver.player_dismissed) {
            const dismissedPlayer = oneOver.batsman;
            const bowlerName = oneOver.bowler;
            if (noOfTimesDismissedByOther[dismissedPlayer]) {
                if (noOfTimesDismissedByOther[dismissedPlayer][bowlerName]) {
                    noOfTimesDismissedByOther[dismissedPlayer][bowlerName] += 1;
                }
                else {
                    noOfTimesDismissedByOther[dismissedPlayer][bowlerName] = 1;
                }
            }
            else {
                noOfTimesDismissedByOther[dismissedPlayer] = {};
                noOfTimesDismissedByOther[dismissedPlayer][bowlerName] = 1;
            }
        }
    }

    let heighestNoOTimesDismissedByOther;
    let maxTime = 0;
    for (wickets in noOfTimesDismissedByOther) {
        const bowlerAndTimes = noOfTimesDismissedByOther[wickets];
        for (bowlerName in bowlerAndTimes) {
            if (bowlerAndTimes[bowlerName] > maxTime) {
                maxTime = bowlerAndTimes[bowlerName];
                heighestNoOTimesDismissedByOther = bowlerAndTimes[bowlerName];
            }

        }
    }

    try {
        fs.writeFileSync("../public/output/heighest-no-of-times-dismissed.json", JSON.stringify(heighestNoOTimesDismissedByOther, null, 2));
        console.log("output file is created");
    }
    catch (error) {
        console.log("error in creating the file", error.message);
    }



}
findHeighestNoOfTimesDismissed();